package com.gl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import com.gl.model.Books;
import com.gl.repository.BookRepository;
import com.gl.service.BooksRead;

	@Service
	public class BooksReadImpl implements BooksRead {

	    @Autowired
	    BookRepository repo;

	    @Override
    	public Books addBooks(Books book) {
        return repo.save(book);
    	}

		@Override
	    public List<Books> getAllBooks() {
	        return repo.findAll();
	    }

	    @Override
	    public Books getBooksById(int id) {
	        return repo.findById(id).get();
	    }

	    @Override
	    public List<Books> getBooksByName(String bookName) {
	        return repo.findByBookName(bookName);
	    }

	    @Override
	    public List<Books> getBooksByAuthor(String author) {
	        return repo.findByAuthor(author);
	    }

	    @Override
	    public Books getBooksByLastAdded() {
	        List<Books> books=repo.findAll();
        return books.get(books.size()-1);

	    }

	    @Override
	    public List<Books> getBooksByAscendingOrder() {
	        return repo.findAll(Sort.by("bookName"));
	    }

		@Override
		public void deleteById(int id) {
			repo.deleteById(id);
		}
	

		@Override
    	public void deleteAllBooks() {
		repo.deleteAll();
    }


	
	}
